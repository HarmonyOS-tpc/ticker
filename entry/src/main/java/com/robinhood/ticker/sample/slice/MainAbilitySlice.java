/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.robinhood.ticker.sample.slice;

import static com.robinhood.ticker.sample.utils.Constants.ALPHABETLIST;
import static com.robinhood.ticker.sample.utils.Constants.RANDOM;

import com.robinhood.ticker.TickerView;
import com.robinhood.ticker.sample.ResourceTable;
import com.robinhood.ticker.sample.utils.Constants;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * MainAbilitySlice extends AbilitySlice
 */
public class MainAbilitySlice extends AbilitySlice {
    private EventHandler handler = new EventHandler(EventRunner.create());

    private boolean isActive;

    private TickerView ticker1;

    private TickerView ticker2;

    private TickerView ticker3;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        Component component = findComponentById(ResourceTable.Id_ticker1);
        if (component instanceof TickerView) {
            ticker1 = (TickerView) component;
            ticker1.setPreferredScrollingDirection(TickerView.ScrollingDirection.DOWN);
        }
        component = findComponentById(ResourceTable.Id_ticker2);
        if (component instanceof TickerView) {
            ticker2 = (TickerView) component;
            ticker2.setPreferredScrollingDirection(TickerView.ScrollingDirection.UP);
        }
        component = findComponentById(ResourceTable.Id_ticker3);
        if (component instanceof TickerView) {
            ticker3 = (TickerView) component;
            ticker3.setPreferredScrollingDirection(TickerView.ScrollingDirection.ANY);
        }
    }

    @Override
    public void onActive() {
        super.onActive();
        isActive = true;
        handler.postTask(createRunnable(), Constants.DELAY_TIME);
    }

    private void onUpdate() {
        final int digits = RANDOM.nextInt(2) + 6;
        ticker1.setText(getRandomNumber(digits));
        final String currencyFloat = Float.toString(RANDOM.nextFloat() * 100);
        ticker2.setText("$" + currencyFloat.substring(0, Math.min(digits, currencyFloat.length())));
        ticker3.setText(generateChars(digits));
    }

    @Override
    protected void onInactive() {
        isActive = false;
        super.onInactive();
    }

    private Runnable createRunnable() {
        return () -> {
            onUpdate();
            if (isActive) {
                handler.postTask(createRunnable(), RANDOM.nextInt(Constants.INT_1750) + Constants.INT_250);
            }
        };
    }

    private String generateChars(int numDigits) {
        final char[] results = new char[numDigits];
        for (int index = 0; index < numDigits; index++) {
            results[index] = ALPHABETLIST.charAt(RANDOM.nextInt(ALPHABETLIST.length()));
        }
        return new String(results);
    }

    private String getRandomNumber(int digits) {
        final int digitsInPowerOf10 = (int) Math.pow(10, digits);
        return Integer.toString(
            RANDOM.nextInt(digitsInPowerOf10) + digitsInPowerOf10 * (RANDOM.nextInt(Constants.INT_8) + 1));
    }
}
