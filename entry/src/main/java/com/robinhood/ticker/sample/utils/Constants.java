/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.robinhood.ticker.sample.utils;

import java.security.SecureRandom;

/**
 * Constants
 */
public class Constants {
    /**
     * Delay time in milisec
     */
    public static final int DELAY_TIME = 1000;

    /**
     * int Constant
     */
    public static final int INT_1750 = 1750;

    /**
     * int Constant
     */
    public static final int INT_250 = 250;

    /**
     * int Constant
     */
    public static final int INT_8 = 8;

    /**
     * Alphabet list
     */
    public static final String ALPHABETLIST = "abcdefghijklmnopqrstuvwxyz";

    /**
     * Generates random number based on current time.
     */
    public static final SecureRandom RANDOM = new SecureRandom();

    private Constants() {
        /* Do nothing */
    }
}