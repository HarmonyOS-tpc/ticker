# ticker

ticker is a simple openharmony component for displaying scrolling text. The ticker handles smooth animations between strings and also string resizing (e.g. animate from "9999" to "10000").

ticker includes :
below customizations for the text.
* Color
* Size
* AnimationDuration
* AnimationInterpolator
* AnimationDelay
* Font
* Text alignment
* PreferredScrollingDirection
* BlurMaskFilter
* PaintFlags

# Usage Instructions
ticker can be used by instantiating the layout elements programmatically as shown below :

```
<com.robinhood.ticker.TickerView
        ohos:id="$+id:ticker"
        ohos:height="match_content"
        ohos:width="match_content"/>
```
Then add the character array to specify the animation style:

```
TickerView tickerView = (TickerView) findComponentById(ResourceTable.Id_ticker);
tickerView.setCharacterLists(TickerUtils.provideNumberList());
```

Now you can call setText to display your data.

# Installation Instructions

1.For using ticker module in sample app, include the source code and add the below dependencies in entry/build.gradle to generate hap/ticker.har.

```
dependencies {
    implementation project(':ticker')
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    testCompile 'junit:junit:4.12'
    }
```

2.For using ticker in separate application using har file, add the har file in the entry/libs folder and add the dependencies in entry/build.gradle file.

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    testCompile 'junit:junit:4.12'
}
```

3. For using ticker from a remote repository in separate application, add the below dependencies in entry/build.gradle file.

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.har'])
    implementation 'io.openharmony.tpc.thirdlib:ticker:1.0.1'
}
```

# License
```
Copyright 2016 Robinhood Markets, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```