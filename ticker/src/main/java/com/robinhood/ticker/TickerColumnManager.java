/*
 * Copyright (C) 2016 Robinhood Markets, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.robinhood.ticker;

import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * In ticker, each character in the rendered text is represented by {@link TickerColumn}. The
 * column can be seen as column of text in which we can animate from one character to the next
 * by scrolling the column vertically. The {@link TickerColumnManager} is then a
 * manager/convenience class for handling list of {@link TickerColumn} which then combines into
 * the entire string we are rendering.
 *
 * @author Jin Cao, Robinhood
 */
class TickerColumnManager {
    final ArrayList<TickerColumn> tickerColumns = new ArrayList<>();

    private final TickerDrawMetrics metrics;

    private TickerCharacterList[] characterLists;

    private Set<Character> supportedCharacters;

    TickerColumnManager(TickerDrawMetrics metrics) {
        this.metrics = metrics;
    }

    /**
     * Sets the text with given string string array
     *
     * @param characterLists string array of text
     * @inheritDoc TickerView#setCharacterLists
     */
    void setCharacterLists(String... characterLists) {
        this.characterLists = new TickerCharacterList[characterLists.length];
        for (int index = 0; index < characterLists.length; index++) {
            this.characterLists[index] = new TickerCharacterList(characterLists[index]);
        }

        supportedCharacters = new HashSet<>();
        for (int index = 0; index < characterLists.length; index++) {
            supportedCharacters.addAll(this.characterLists[index].getSupportedCharacters());
        }

        // Update character lists in current columns
        for (TickerColumn tickerColumn : tickerColumns) {
            tickerColumn.setCharacterLists(this.characterLists);
        }
    }

    TickerCharacterList[] getCharacterLists() {
        return characterLists;
    }

    /**
     * Tell the column manager the new target text that it should display.
     *
     * @param text string array of text
     */
    void setText(char[] text) {
        if (characterLists == null) {
            throw new IllegalStateException("Need to call #setCharacterLists first.");
        }

        // First remove any zero-width columns
        for (int index = 0; index < tickerColumns.size(); ) {
            final TickerColumn tickerColumn = tickerColumns.get(index);
            if (tickerColumn.getCurrentWidth() > 0) {
                index++;
            } else {
                tickerColumns.remove(index);
            }
        }

        // Use Levenshtein distance algorithm to figure out how to manipulate the columns
        final int[] actions = LevenshteinUtils.computeColumnActions(getCurrentText(), text, supportedCharacters);
        int columnIndex = 0;
        int textIndex = 0;
        for (int index = 0; index < actions.length; index++) {
            switch (actions[index]) {
                case LevenshteinUtils.ACTION_INSERT:
                    if (columnIndex <= tickerColumns.size()) {
                        tickerColumns.add(columnIndex, new TickerColumn(characterLists, metrics));
                    }
                    // Intentional fallthrough
                case LevenshteinUtils.ACTION_SAME:
                    if (columnIndex < tickerColumns.size()) {
                        tickerColumns.get(columnIndex).setTargetChar(text[textIndex]);
                        columnIndex++;
                        textIndex++;
                    }
                    break;
                case LevenshteinUtils.ACTION_DELETE:
                    if (columnIndex < tickerColumns.size()) {
                        tickerColumns.get(columnIndex).setTargetChar(TickerUtils.EMPTY_CHAR);
                        columnIndex++;
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Unknown action: " + actions[index]);
            }
        }
    }

    void onAnimationEnd() {
        for (int index = 0, size = tickerColumns.size(); index < size; index++) {
            final TickerColumn column = tickerColumns.get(index);
            column.onAnimationEnd();
        }
    }

    void setAnimationProgress(float animationProgress) {
        for (int index = 0, size = tickerColumns.size(); index < size; index++) {
            final TickerColumn column = tickerColumns.get(index);
            column.setAnimationProgress(animationProgress);
        }
    }

    float getMinimumRequiredWidth() {
        float width = 0f;
        for (int index = 0, size = tickerColumns.size(); index < size; index++) {
            width += tickerColumns.get(index).getMinimumRequiredWidth();
        }
        return width;
    }

    float getCurrentWidth() {
        float width = 0f;
        for (int index = 0, size = tickerColumns.size(); index < size; index++) {
            if (index < tickerColumns.size() && tickerColumns.get(index) != null) {
                width += tickerColumns.get(index).getCurrentWidth();
            }
        }
        return width;
    }

    char[] getCurrentText() {
        final int size = tickerColumns.size();
        final char[] currentTexts = new char[size];
        for (int index = 0; index < size; index++) {
            currentTexts[index] = tickerColumns.get(index).getCurrentChar();
        }
        return currentTexts;
    }

    /**
     * This method will draw onto the canvas the appropriate UI state of each column dictated
     * by {@param animationProgress}. As side effect, this method will also translate the canvas
     * accordingly for the draw procedures.
     *
     * @param canvas Canvas for text
     * @param textPaint Paint for text
     */
    void draw(Canvas canvas, Paint textPaint) {
        for (int index = 0, size = tickerColumns.size(); index < size; index++) {
            if (index < tickerColumns.size()) {
                final TickerColumn column = tickerColumns.get(index);
                column.draw(canvas, textPaint);
                canvas.translate(column.getCurrentWidth(), 0f);
            }
        }
    }
}