/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.robinhood.ticker.util;

import ohos.agp.components.AttrSet;
import ohos.agp.utils.Color;

/**
 * ResUtil
 */
public class ResUtil {
    private ResUtil() {
    }

    /**
     * Obtains the int from AttrSet read from xml file
     *
     * @param attrSet AttrSet read from xml
     * @param key xml attribute name
     * @param defaultValue Default value
     * @return int
     */
    public static int getIntFromAttrSet(AttrSet attrSet, String key, int defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        return attrSet.getAttr(key).isPresent() ? attrSet.getAttr(key).get().getIntegerValue() : defaultValue;
    }

    /**
     * Obtains the dimension from AttrSet read from xml file
     *
     * @param attrSet AttrSet read from xml
     * @param key xml attribute name
     * @param defaultValue Default value
     * @return float
     */
    public static float getDimenFromAttrSet(AttrSet attrSet, String key, float defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        return attrSet.getAttr(key).isPresent() ? attrSet.getAttr(key).get().getDimensionValue() : defaultValue;
    }

    /**
     * Obtains the string from AttrSet read from xml file
     *
     * @param attrSet AttrSet read from xml
     * @param key xml attribute name
     * @param defaultValue Default value
     * @return String
     */
    public static String getStringFromAttrSet(AttrSet attrSet, String key, String defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        return attrSet.getAttr(key).isPresent() ? attrSet.getAttr(key).get().getStringValue().trim() : defaultValue;
    }

    /**
     * Obtains the color from AttrSet read from xml file
     *
     * @param attrSet AttrSet read from xml
     * @param key xml attribute name
     * @param defaultValue Default value
     * @return color
     */
    public static int getColorFromAttrSet(AttrSet attrSet, String key, int defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        return attrSet.getAttr(key).isPresent() ? attrSet.getAttr(key).get().getColorValue().getValue() : defaultValue;
    }

    /**
     * Obtains the color from AttrSet read from xml file
     *
     * @param attrSet AttrSet read from xml
     * @param key xml attribute name
     * @param defaultValue Default color
     * @return color
     */
    public static Color getColorFromAttrSet(AttrSet attrSet, String key, Color defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        return attrSet.getAttr(key).isPresent() ? attrSet.getAttr(key).get().getColorValue() : defaultValue;
    }
}