/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.robinhood.ticker.util;

/**
 * TextUtils
 */
public class TextUtils {
    private TextUtils() {
    }

    /**
     * Checks if the string is null or empty
     *
     * @param str string input
     * @return true if the input string is not valid
     */
    public static boolean isEmpty(CharSequence str) {
        return str == null || str.length() == 0;
    }

    /**
     * Checks if the input string values are equal
     *
     * @param char1 string input 1
     * @param char2 string input 2
     * @return true if input string values are equal
     */
    public static boolean equals(CharSequence char1, CharSequence char2) {
        if (char1 == char2) {
            return true;
        }
        if (char1 != null && char2 != null && (char1.length()) == char2.length()) {
            if (char1 instanceof String && char2 instanceof String) {
                return char1.equals(char2);
            } else {
                for (int index = 0; index < char1.length(); index++) {
                    if (char1.charAt(index) != char2.charAt(index)) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }
}